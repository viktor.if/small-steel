const path = require('path');
const { config } = require('./wdio.shared.conf');

//
// ====================
// Port
// ====================
//
config.specs = 4723;
//
// ====================
// Specs
// ====================
//
config.specs = [
    './test/specs/ios-FindElements.spec.js'
];
//
// ====================
// Capabilities
// ====================
//
config.capabilities = [
    {
        platformName: "iOS",
        "appium:platfromVersion": "14.5",
        "appium:deviceName": "iPhone 8",
        "appium:automationName": "XCUITest",
        "appium:app": path.join(process.cwd(), "./app/ios/MVCTodo.app"),
        "appium:autoGrantPermissions": true,
        "appium:noRest": true
    }
];
//
// Test runner services
// Services take over a specific job you don't want to take care of. They enhance
// your test setup with almost no effort. Unlike plugins, they don't add new
// commands. Instead, they hook themselves up into the test process.
config.services =  [['appium', {
    args: {
        address: 'localhost',
        port: 4723,
        relaxedSecurity: true,
    },
    logPath: './'
}]];

exports.config = config;
