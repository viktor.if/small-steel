class ItemScreen {
    get createItem() {
        return $('//@[name="Create item"]');
    }

    get title() {
        return $('//@[value="Title"]');
    }

    get dueDate() {
        return $('//@[value="Due"]');
    }

    get datePicker() {
        return $('~Date Picker');
    }

    get createBtn() {
        return $('~Create')
    }

    get secondWindow() {
        return $('//XCUIElementTypeWindow');
    }

    getByAccessibility(name) {
        return $(`~${name}`);
    }
}

module.exports = new ItemScreen();
