class DeleteNoteScreen {
    get optionsBtn() {
        return $('~More');
    }

    get deleteNoteOption() {
        return $('//*[@text="Delete"]');
    }

    get alertConfirmationBtn() {
        return $('//*[@text="OK"]');
    }

    get burgerMenuBtn() {
        return $('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/icon_nav"]');
    }

    get trashBinMenuBtn() {
        return $('//*[@text="Trash Can"]');
    }

    get deletedNote() {
        return $('//*[@text="Fav Anime List"]');
    }
}

export default new DeleteNoteScreen();
