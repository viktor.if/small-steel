describe('Add Notes', () => {
    it('Skip the tutorial', async () => {
        await $('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/btn_start_skip"]').click();
        await expect($('//*[@text="Add note"]')).toBeDefined();
    });

    it("Add, save and verify a note", async () => {
        await $('//*[@text="Add note"]').click();
        await $('//*[@text="Text"]').click();

        await expect($('//*[@text="Editing"]')).toBeDefined();

        // add note title
        await $('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/edit_title"]').addValue('Fav Anime List');

        // add note body
        await $('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/edit_note"]').addValue('Naruto\nOnePiece\nAOT');

        // save changes
        await driver.back();
        await driver.back();

        // asseertion
        await expect($('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/edit_btn"]')).toBeDisplayed();
        await expect($('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/view_note"]')).toHaveText('Naruto\nOnePiece\nAOT');
    });

    it('Delete a note', async () => {
        // delete not
        await $('~More').click();
        await $('//*[@text="Delete"]').click();
        await $('//*[@text="OK"]').click();

        // 1st assertion
        await expect($('//*[@text="Add note"]')).toBeDisplayed();

        // open trash open
        await $('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/icon_nav"]').click();
        await $('//*[@text="Trash Can"]').click();

        // 2nd assertion
        await expect($('//*[@text="Fav Anime List"]')).toBeDisplayed();
    })
});
