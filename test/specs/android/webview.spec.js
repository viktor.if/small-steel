import AddNoteScreen from "../../screenobjects/android/add-note.screen";

describe('Web Browser Access', () => {
    before(async () => {
        await AddNoteScreen.skipBtn.click();
        await expect(AddNoteScreen.addNoteTxt).toBeDefined();
    });

    it('Access external link and verify content in the browser', async () => {
        // click on the nav icon
        await $('//*[@resource-id="com.socialnmobile.dictapps.notepad.color.note:id/icon_nav"]').click();

        // click on the facebook link
        await $('//*[@text="Like us on Facebook"]').click();

        // console.log(await $$('.img'))
        // await driver.pause(5000)

        // get current context
        // await driver.getContext()

        // get all contexts
        console.log(await driver.getContexts())

        // switch to web view context
        await driver.switchContext('WEBVIEW_chrome')
        
        // assert the cover image is displayed
        // const coverImg = await $('//android.view.View[@content-desc="Cover Photo: ColorNote\'s photo."]');
        const coverImg = await $('.img.coverPhoto');
        await expect(coverImg).toBeDisplayed();

        // switch back to the app
        await driver.switchContext('NATIVE_APP')
        await driver.back()

        // continue work
        await $('//*[@text="Notes"]').click()

        const addNoteText = await $('//*[@text="Add note"]')
        await expect(addNoteText).toBeDisplayed()
    });
})
