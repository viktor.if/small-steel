import AddNoteScreen from "../../screenobjects/android/add-note.screen";
import DeleteNoteScreen from "../../screenobjects/android/delete-note.screen";

describe('Add Notes', () => {
    it('Skip the tutorial', async () => {
        await AddNoteScreen.skipBtn.click();
        await expect(AddNoteScreen.addNoteTxt).toBeDefined();
    });

    it("Add, save and verify a note", async () => {
        await AddNoteScreen.addNoteTxt.click();
        await AddNoteScreen.textOption.click();

        await expect(AddNoteScreen.textEditing).toBeDefined();

        // add note title
        await AddNoteScreen.noteHeading.addValue('Fav Anime List');

        // add note body
        await AddNoteScreen.noteBody.addValue('Naruto\nOnePiece\nAOT');

        // save changes
        await AddNoteScreen.saveNote();

        // asseertion
        await expect(AddNoteScreen.editBtn).toBeDisplayed();
        await expect(AddNoteScreen.viewNote).toHaveText('Naruto\nOnePiece\nAOT');
    });

    it('Delete a note', async () => {
        // delete not
        await DeleteNoteScreen.optionsBtn.click();
        await DeleteNoteScreen.deleteNoteOption.click();
        await DeleteNoteScreen.alertConfirmationBtn.click();

        // 1st assertion
        await expect(AddNoteScreen.addNoteTxt).toBeDisplayed();
        
        // open trash open
        await DeleteNoteScreen.burgerMenuBtn.click();
        await DeleteNoteScreen.trashBinMenuBtn.click();

        // 2nd assertion
        await expect(DeleteNoteScreen.deletedNote).toBeDisplayed();
    })
});
