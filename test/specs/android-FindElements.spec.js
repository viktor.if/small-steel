// $ - find single element
// $$ - find all elements

describe('Android Elements Test', () => {
    it('Find element by accessibility id', async () => {
        const appOption = await $('~App');
        await appOption.click();
        const actionBar = await $('~Action Bar');
        await expect(actionBar).toBeExisting();
    });

    it('Find element by class name', async () => {
        const className = await $('android.widget.TextView');
        await expect(className).toHaveText('API Demos');
    });

    xit('Find elements by Xpath', async () => {
        await $('//android.widget.TextView[@content-desc="Alert Dialogs"]').click();
        await $('//android.widget.Button[@resource-id="io.appium.android.apis:id/select_button"]').click();
        await $('//android.widget.TextView[@text="Command two"]').click();
        const textAssertion = await $('//android.widget.TextView');
        await expect(textAssertion).toHaveText("You selected: 1 , Command two")
    });

    xit('Find elements by UIAutomator', async () => {
        await $('android=new UiSelector().textContains("Alert")').click();
    });

    it('Find multiple elements', async () => {
        const actualList = [];
        const expectedList = [
            'API Demos', "Access'ibility", 'Accessibility',
            'Animation', 'App',
            'Content', 'Graphics',
            'Media', 'NFC',
            'OS', 'Preference',
        ];
        
        // find multiple elements
        const  textList = await $$('android.widget.TextView');

        // loop through them
        for (const element of textList) {
            actualList.push(await element.getText());
        }
        console.log(actualList)

        // assert the list
        await expect(actualList).toEqual(expectedList);
    });

    it.only('Find and validate the input field', async () => {
        // await $('android.widget.ListView').scrollIntoViewOptions({ block: 'center', inline: 'center' })

        // find Views component and click on it
        // await $('~Views').scrollIntoViewOptions({ block: 'center' })
        await $('~Views').click();

        // find Auto Complete component and click on it
        await $('//android.widget.TextView[@content-desc="Auto Complete"]').click();

        // find Screen top text and click on it
        await $('android.widget.TextView').click();

        const textField = await $('//*[@resource-id="io.appium.android.apis:id/edit"]');
        await textField.addValue('Canada');

        // verify the country name
        await expect(textField).toHaveText('Canada');
    });
});
