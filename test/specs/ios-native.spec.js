describe('iOS Native Features', () => {
    it('Work with alert box', async () => {
        await $('~Alert Veiws').click();
        await $('~Okay / Cancel').click();

        console.log(await driver.getAlertText());

        // Click OK
        // await $('~OK').click();
        await driver.dismissAlert();

        await expect($('~OK')).not.toExist(); 
    });

    it('Work with scrollable elements', async () => {
        // easiest
        // await driver.execute('mobile: scroll', { direction: 'down' });
        // await driver.execute('mobile: scroll', { direction: 'up' });
        
        // complex
        await $('~Picker View').click();

        const redPicker = await $('~Red color component value');
        await driver.execute('mobile: scroll', { element: redPicker.elementId, direction: 'down' });
    });

    it('Work with the Picker View', async () => {
        const redPicker = await $('~Red color component value');
        const greenPicker = await $('~Green color component value');
        const bluePicker = await $('~Blue color component value');

        // set purple color (125, 0, 125);
        await redPicker.addValue('125');
        await greenPicker.addValue('0');
        await bluePicker.addValue('125');
    });
});
