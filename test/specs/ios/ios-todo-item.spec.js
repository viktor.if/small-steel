describe('Todo Item', () => {
    it('Create a Todo Item', async () => {
        // Create TODO List
        await $('//*[@name="Create List"]').click();
        await $('//*[@value="List Name"]').addValue('Things to do today');
        await $('~Create').click();
        await expect(await $('~Things to do today')).toExist();

        // Create TODO Item
        await $('~Things to do today').click();
        await $('//@[name="Create item"]').click();
        await $('//@[value="Title"]').addValue('Buy groceries');
        await $('//@[value="Due"]').click();
        await $('~Date Picker').click();
        await $('~Sunday, Novemberf 28').click();
        await $('//XCUIElementTypeWindow').click();
        await $('~Create').click();

        // Assertion
        await expect(await $('//*[@text="Buy groceries"]')).toExist();
        await expect(await $('//*[@text="Sunday, Novemberf 28"]')).toExist();
    });
});
