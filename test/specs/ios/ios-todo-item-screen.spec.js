const ListScreen = require('../../screenobjects/ios/list.screen');
const ItemScreen = require('../../screenobjects/ios/item.screen');

describe('Todo Item', () => {
    before(async () => {
        // Create TODO List
        await ListScreen.createListBtn.click();
        await ListScreen.listNameInput.addValue('Things to do today');
        await ListScreen.createBtn.click();
        await expect(await ListScreen.listNameField('Things to do today')).toExist();
        await ListScreen.listNameField('Things to do today').click();
    });

    it('Create a Todo Item', async () => {
        // Create TODO Item
        await ItemScreen.createItem.click();
        await ItemScreen.title.addValue('Buy groceries');
        await ItemScreen.dueDate.click();
        await ItemScreen.datePicker.click();
        await ItemScreen.getByAccessibility('Sunday, Novemberf 28').click();
        await ItemScreen.secondWindow.click();
        await ItemScreen.createBtn.click();

        // Assertion
        await expect(await ItemScreen.getByAccessibility('Buy groceries')).toExist();
        await expect(await ItemScreen.getByAccessibility('Sunday, Novemberf 28')).toExist();
    });
});
