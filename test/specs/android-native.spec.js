describe('Android Native Feature Tests', async () => {
    it('Access an Activity directly', async () => {
        // access activity
        await driver.startActivity("io.appium.android.apis", "io.appium.android.apis.app.AlertDialogSamples")
        
        // pause 3s
        await driver.pause(3000);
        
        // assertion
        await expect($('//*[@text="App/Alert Dialogs"]')).toExist();
    });

    it('Working with Dialog Boxes', async () => {
        // access activity
        await driver.startActivity("io.appium.android.apis", "io.appium.android.apis.app.AlertDialogSamples");
        
        // click on the first dialog
        await $('//*[@resource-id="io.appium.android.apis:id/two_buttons"]').click();

        // accept Alert
        // await driver.acceptAlert();
        
        // dismiss Alert
        // await driver.dismissAlert();

        // click on the OK button
        await $('//*[@resource-id="android:id/button1"]').click();

        // assertion - alert box is not visible anymore
        await expect($('//*[@resource-id="android:id/alertTitle"]')).not.toExist();
    });

    it('Vertical scrolling', async () => {
        await $('~App').click();
        await $('~Activity').click();

        // scroll to the end (not stable)
        // await $('android=new UiScrollable(new UiSelector().scrollable(true)).scrollToEnd(1, 5)');
        
        // scrollTextIntoView - more stable
        await $('android=new UiScrollable(new UiSelector().scrollable(true)).scrollTextIntoView("Secure Surfaces")').click()

        // await $('~Secure Surfaces').click();

        // assertion
        await expect($('~Secure Dialog')).toExist();
    });

    it('Horizontal scrolling', async () => {
        driver.startActivity('io.appium.android.apis', 'io.appium.android.apis.view.Gallery1');

        await $('android=new UiScrollable(new UiSelector().scrollable(true)).setAsHorizontalList().scrollForward()').click();
        await $('android=new UiScrollable(new UiSelector().scrollable(true)).setAsHorizontalList().scrollBackward()').click();
        await driver.pause(3000);
    });

    it.only('Scrolling exercise', async () => {
        // Open dialog
        await $('android=new UiScrollable(new UiSelector().scrollable(true)).scrollTextIntoView("Views")').click();
        await $('~Date Widgets').click();
        await $('~1. Dialog').click();

        // Get current date
        const currDate = await $('//*[@resource-id="io.appium.android.apis:id/dateDisplay"]').getText();
        console.log({ currDate});

        // Open date picker
        await $('~change the date').click();

        // Scroll right
        await $('android=new UiScrollable(new UiSelector().scrollable(true)).setAsHorizontalList().scrollForward()').click();

        // Select 10 day
        await $('//*[@text="10"]').click();

        // Click OK
        await $('//*[@text="OK"]').click();

        // Get new date
        const newDate = await $('//*[@resource-id="io.appium.android.apis:id/dateDisplay"]').getText();

        // Assertion
        await expect(newDate).not.toEqual(currDate);
    });
});